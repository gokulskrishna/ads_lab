#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>

int no_of_nodes=0;
struct Node
{
    int data;
    char color;
    struct Node *parent;
    struct Node *rchild;
    struct Node *lchild;
}*p = NULL,*delNode = NULL;

struct Node *search(int element)
{
    struct Node *root = p, *nodeSearched;
    int i = 0;
    bool found = false;
    while (root != NULL)
    { 
        if (element == root->data)
        {
            printf("\n%d is element found",root->data);
            found = true;
            nodeSearched =  root;
            break;
        }
       
        if (element < root->data)root = root->lchild; 
        else root = root->rchild; 
        i++; // incrementing level
    }
    if (found)
    {
        printf("\n%d found at level %d\n", element, i);
        return nodeSearched; 
    }
    else
    {
        printf("\n%d not found\n", element);
        return NULL;
    }
}

void leftToRightRotate(struct Node *root)
{
    struct Node *grandParent = ((root->parent)->parent), *P = root->parent;
    if(root->parent==NULL) printf("\nparent is null"); printf("\n%d is value at parent",P->data);
    if(grandParent->parent == NULL) 
    {
        printf("\ntree starting found");
        p = P; // setting root as parent
        grandParent->lchild = P->rchild = NULL;
        p->rchild = grandParent;
        grandParent->parent = P;
    }
    else{
        printf("\n %d is not tree starting",grandParent->data);
        struct Node *temp = grandParent->parent;
        grandParent->lchild = P->rchild;
        P->rchild = grandParent;
        if(P->data < temp->data)    temp->lchild = P;
        else    temp->rchild = P;
    }
}

void leftToLeftRotate(struct Node *root)
{
    struct Node *grandParent = ((root->parent)->parent), *P = root->parent;
    root->parent = grandParent;
    grandParent->lchild = root;
    root->lchild = P;
    P->parent = root;
    P->rchild = NULL;
}

void rightToRightRotate(struct Node *root)
{
    struct Node *grandParent = ((root->parent)->parent), *P = root->parent;
    root->parent = grandParent;
    grandParent->rchild = root;
    root->rchild = P;
    P->parent = root;
    P->lchild = NULL;
}

void rightToLeftRotate(struct Node *root)
{
    struct Node *grandParent = ((root->parent)->parent), *P = root->parent;
    if(grandParent->parent == NULL) 
    {
        printf("\ntree starting found");
        p = P; // setting root as parent
        grandParent->rchild = p->lchild;
        p->lchild = grandParent;
        grandParent->parent = P;
        P->parent = NULL;
    }
    else{
        printf("\n %d is not tree starting",grandParent->data);
        struct Node *temp = grandParent->parent;
        P->parent = temp;
        grandParent->rchild = P->lchild ;
        P->lchild = grandParent;
        grandParent->parent = P;
        if(P->data < temp->data)    temp->lchild = P;
        else    temp->rchild = P;
    }
}

void nullize(struct Node *root,bool addNullNode){
    if(addNullNode){
        if(root!=NULL){
            struct Node *nullNode = malloc(sizeof(struct Node));
            nullNode->color = 'b';
            root->rchild = root->lchild = nullNode;
            return;
        }

    }
    else{

    }
}

struct Node *insert(struct Node *root, int item) 
{
    struct Node *temp;
    if (root == NULL)
    {
        struct Node *node = malloc(sizeof(struct Node));

        node->data = item;

        if (no_of_nodes == 0){
            p = node;
            node->color = 'b';
            node->parent = NULL;
        }
        else node->color = 'r';

        no_of_nodes++;
        return node;
    }
    if (item < root->data)
    {
        temp = insert(root->lchild, item);
        root->lchild = temp;
        temp->parent = root;
    }
    else
    {
        temp = insert(root->rchild, item);
        root->rchild = temp;
        temp->parent = root;
    }
    return root;
}


void display(struct Node *node, int level)
{
    if (node != NULL)
    {
        display(node->rchild, level + 1);
        printf("\n");
        for (int i = 0; i < level; i++)
        {
            printf("    ");
        }
        if(node->data) printf("%d",node->data);
        printf(" %c",node->color);
        display(node->lchild, level + 1);
    }
}

struct Node *findMin(struct Node *root)
{
    struct Node *temp = root;
    while (temp->lchild != NULL)
    {       
        temp = temp->lchild;
    }
    return temp;
}

int inorder(struct Node *root)
{
    struct Node *temp = root;
    if (temp == NULL) return 0;
    else
    {
        inorder(temp->lchild);
        printf(" %d ", temp->data);
        inorder(temp->rchild);
    }
}
struct Node* sibiling(struct Node *root)
{
    if(root->data < (root->parent)->data)  return (root->parent)->rchild;
    else   return (root->parent)->lchild;
} 

void recolor(struct Node *root){
    printf("\nInside recolor fn");
    struct Node *grandParent = ((root->parent)->parent), *P = root->parent , *uncle=sibiling(root->parent);
    if(grandParent!=NULL) {
        grandParent->color = 'r';
        P->color = 'b';
    }
    if(uncle!=NULL) uncle->color = P->color = 'b';
}

void validateInsertion(struct Node* root)
{
    if((p!=NULL && p->color == 'r') || (root->parent != NULL && root->parent->color == 'r' && root->color=='r'))
    {
        if(p->color=='r')
        {
            p->color = 'b';
            return;
        }
        if (root->parent != NULL && (root->parent)->parent != NULL)
        {
            struct Node *grandParent = ((root->parent)->parent), *P = root->parent ,*uncle=sibiling(root->parent);
            bool isBlackUncle = true;

            if(uncle!=NULL) isBlackUncle = (uncle->color == 'b')? true:false; 
            if(uncle!=NULL && uncle->color=='r'){
                printf("\n%d is red node",uncle->data);
                recolor(root);
                validateInsertion(grandParent);
                return;
            }
            if(uncle==NULL || isBlackUncle)
            {
                if(root->data >= P->data && P->data >= grandParent->data){
                    printf("right skewed");
                    recolor(root);
                    rightToLeftRotate(root);
                }
                if(root->data < P->data && P->data < grandParent->data){
                    printf("left skewed");
                    recolor(root);
                    leftToRightRotate(root);
                }
                if(root->data >= P->data && P->data < grandParent->data){
                    printf("\nparent is left child and root is right child");
                    leftToLeftRotate(root);
                    printf("\nleftToLeftRotate successfull");
                    validateInsertion(root->lchild); 
                }
                if(root->data < P->data && P->data >= grandParent->data){
                    printf("\nparent is right child and  root is left child");
                    rightToRightRotate(root);
                    printf("\nrightToRightRotate successfull");
                    validateInsertion(root->rchild); 
                }
            }
        }
    }
}

struct Node *findMin(struct Node *root)
{
    struct Node *temp = root;
    while (temp->lchild != NULL)
    {
        temp = temp->lchild;
    }
    return temp;
}

struct Node* delete(struct Node* root, int key)
{

    if (root == NULL){
        no_of_nodes--;
        if(no_of_nodes==0){
            p=NULL;
        }
        return root;
    }
    if (key < root->data)
        root->lchild = delete(root->lchild, key);
    else if (key >= root->data)
        root->rchild = delete(root->rchild, key);
    if (root->lchild == NULL) {
        struct Node* temp = root->rchild;
        free(root);
        return temp;
    }
    else if (root->rchild == NULL) {
        struct Node* temp = root->lchild;
        free(root);
        return temp;
    }
    struct Node* temp = findMin(root->rchild);
    root->data = temp->data;
    root->rchild = delete(root->rchild, temp->data);
    return root;
}

struct Node* delete(struct Node* root, int key)
{
 
    if (root == NULL){
        no_of_nodes--;
        if(no_of_nodes==0){
            p=NULL;
        }
        return root;
    }
    if (key < root->data)
        root->lchild = delete(root->lchild, key);
    else if (key > root->data)
        root->rchild = delete(root->rchild, key);
    else {
        
        if (root->lchild == NULL) {
            struct Node* temp = root->rchild;
            free(root);
            return temp;
        }
        else if (root->rchild == NULL) {
            struct Node* temp = root->lchild;
            free(root);
            return temp;
        }
        struct Node* temp = findMin(root->rchild);
        root->data = temp->data;
        root->rchild = delete(root->rchild, temp->data);
    }
    return root;
}

void validateDeletion(struct Node *root){

    struct Node *P = root->parent,*grandparent =(root->parent)->parent, *uncle = sibiling(root->parent);
    printf("\n%d is node to be deleted",root->data);
    printf("\n%c is node's color",root->color);
    if(root->parent!=NULL)  printf("\n%d is node's parent",(root->parent)->data);
    if(root->lchild!=NULL)  printf("\n%d is node's parent",(root->lchild)->data);
    if(root->rchild!=NULL)  printf("\n%d is node's parent",(root->rchild)->data);
    if(root->color=='r') {
        
    }
}
void main()
{
    int menuInput = 1, element;
    while (menuInput != 0)
    {
        printf("\n*********** Menu ***********");
        printf("\n1 - Enter element");
        printf("\n2 - Delete element");
        printf("\n3 - Search element ");
        printf("\n4 - Display tree ");
        printf("\n5 - Find maximum");
        printf("\n6 - Find minimum");
        printf("\n7 - Inorder traversal");
        printf("\n8 - Left to Right Rotate");
        printf("\n0 - Exit");
        printf("\nEnter your choice : ");
        scanf("%d", &menuInput);
        switch (menuInput)
        {
            case 0:
                break;
            case 1:
            {
                printf("\nEnter element : ");
                scanf("%d", &element);
                insert(p, element);
                validateInsertion(search(element));
                display(p,0);
                break;
            }
            case 2:
            {
                if(p==NULL){
                    printf("\nNo tree found");
                    break;
                }
                int element;
                struct Node *node;
                printf("\nEnter element to delete : ");
                scanf("%d", &element);
                node = search(element);
                if(node==NULL){
                    printf("\n%d not found", element);
                }
                else{
                    delNode = malloc(sizeof(struct Node));
                    *delNode = *node;
                    // delNode->data = node->data;
                    // delNode->color = node->color;
                    // delNode->rchild = node->rchild;
                    // delNode->lchild = node->lchild;
                    // delNode->parent = node->parent;
                    delete (p,element);
                    validateDeletion(delNode);
                }
                break;
            }
            case 3:
            {
                int element;
                printf("\nEnter element to be searched : ");
                scanf("%d", &element);
                search(element);
                break;
            }
            case 4:
            {
                printf("\nParent (at left) to children (at right)");
                display(p, 0);
                break;
            }
            case 5:break;
            case 6:break;
            case 7:
            {
                inorder(p);
                break;
            }
            case 8:{
                int value;
                printf("\nEnter leaf element : ");
                scanf("%d", &value);
                struct Node *node = search(value);
                printf("\nvalue of node is %d",node->data);
                rightToRightRotate(node);
                break;
            }
            default: printf("\nInvalid input!");
        }
    }
}
